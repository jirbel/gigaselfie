const del = require('del');

var gulp = require('gulp');
var gulpSequence = require('gulp-sequence'); // chytre razeni tasku
var less = require('gulp-less');
var lessImport = require('gulp-less-import');
var path = require('path');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var cssshrink = require('gulp-cssshrink');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var gutil = require('gulp-util');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var rimraf = require('gulp-rimraf');
var replace = require('gulp-replace');
var mergeStream = require('merge-stream');
var spritesmith = require('gulp.spritesmith');
var php2html = require("gulp-php2html");
var autoprefixer = require('gulp-autoprefixer');
var fs = require('file-system');
var fse = require('fs-extra');
var merge = require('merge-stream');
var browserSync = require('browser-sync'); // lze pouzit pouzit pouze lokalne (browsersync.io)
// defaultne je browserSync deaktivovany (vcetne jeho soucasti), pokud ho chces lokalne pouzivat, tak zakomentuj "logSnippet" v nastaveni browsersync
// z konzole pak zkopirujes vygenerovany <script>, ktery vlozis do layoutu (idealne do podminky, pokud neni produkcni mod)
// nezapomen zde odkomentovat inicializaci tasku v default launcheru, reloady a notifikace


// global paths
var paths = {
    base: "./",
    // nepouzivej na watch cesty ./
    // watch pak nehlida pridavani/mazani souboru!
    watch: {
        js: "src/js/**/*.js",
        less: "src/less/**/*.less",
        favicons: "src/favicons/*.*",
        php2html: [
            "src/*.php",
            "src/parts/*.php"
        ],
        images: [
            "src/images/**/*",
            "src/sprites/*"
        ]
    },
    fonts: {
        clean: "dist/fonts/**/*",
        dest: "dist/fonts",
        src: [
            "bower_components/bootstrap/fonts/*.*",
            "bower_components/font-awesome/fonts/*.*",
            "src/fonts/**"
        ]
    },
    scripts: {
        name: "scripts.js",
        stage: "scripts.min.js",
        dest: "dist/js",
        src: [
            "bower_components/bootstrap/js/affix.js",
            "bower_components/bootstrap/js/alert.js",
            "bower_components/bootstrap/js/button.js",
            "bower_components/bootstrap/js/carousel.js",
            "bower_components/bootstrap/js/collapse.js",
            "bower_components/bootstrap/js/dropdown.js",
            "bower_components/bootstrap/js/modal.js",
            "bower_components/bootstrap/js/tooltip.js",
            "bower_components/bootstrap/js/popover.js",
            "bower_components/bootstrap/js/scrollspy.js",
            "bower_components/bootstrap/js/tab.js",
            "bower_components/bootstrap/js/transition.js",
            "bower_components/jquery.scrollTo/jquery.scrollTo.min.js",
            "bower_components/fancybox/source/jquery.fancybox.pack.js",
            "bower_components/fancybox/source/helpers/jquery.fancybox-thumbs.js",
            "bower_components/owl.carousel/dist/owl.carousel.min.js",
            // "bower_components/jquery-ui/jquery-ui.min.js",
            "node_modules/picturefill/dist/picturefill.min.js",
            "src/js/*.js",
            "src/js/*/*.js",
            "!dist/jquery/*.js"
        ],
        jquery: {
            paths: [
                {
                    src: "bower_components/jquery-legacy/dist/jquery.min.js",
                    dest: "dist/js/jquery/",
                    name: 'jquery.legacy.js'
                },
                {
                    src: "bower_components/jquery-modern/dist/jquery.min.js",
                    dest: "dist/js/jquery/",
                    name: 'jquery.modern.js'
                }
            ]
        }
    },
    less: {
        stage: "styles.min.css",
        dest: "dist/css",
        src: "src/less/styles.less"
    },
    images: {
        clean: "dist/images/**/*.*",
        dest: "dist/images",
        src: "src/images/**/*.*"
    },
    sprites: {
        src: "src/sprites/*.png",
        url: "../images/sprites/sprites.png",
        images: {
            name: "sprites.png",
            dest: "src/images/sprites/"
        },
        styles: {
            name: "sprites.less",
            dest: "src/less/components/sprites/"
        }
    },
    favicons: {
        dest: "dist/favicons",
        src: "src/favicons/*.*"
    },
    sablona: {
        less: {
            dest: "src/less/bems/",
            src: "../sablony/src/sablony/"
        },
        php: {
            dest: "src/",
            src: "../sablony/src/sablony/"
        },
        js: {
            dest: "src/js/tmpls/",
            src: "../sablony/src/sablony/"
        }
    },
    fancybox: {
        images: {
            dest: "dist/images/fancybox",
            src: [
                "bower_components/fancybox/source/**/*.gif",
                "bower_components/fancybox/source/**/*.jpg",
                "bower_components/fancybox/source/**/*.png"
            ]
        },
        preprocess: {
            dest:   "dist/preprocess/fancybox-preprocess.css",
            src:    "bower_components/fancybox/source/**/*.css"
        }
    },
    owlcarousel: {
        dest:   "dist/css/plugins/owl.carousel",
        src:    "bower_components/owl.carousel/dist/assets/**/*.*"
    },
    php2html: {
        src: "src/*.php",
        dest: "dist/"
    },
    autoprefixer: {
        src: "dist/styles.min.css",
        dest: "dist/"
    }
};


// "gulp" spusti default task
// "gulp stage" spusti kompilaci pro produkci
// launcher
gulp.task('default', gulpSequence('scripts', 'less', 'images', 'fonts',  'favicons', 'fancybox-images', 'owlcarousel', 'php2html', 'browsersync-init', 'watch'));
gulp.task('setup', gulpSequence('scripts', 'jquery', 'less', 'images', 'fonts', 'favicons', 'php2html'));
gulp.task('stage', gulpSequence('stageless', 'stagejs', 'autoprefixer'));


// browserSync init
gulp.task('browsersync-init', function () {
    browserSync.init({
        reloadOnRestart: true,
        //logSnippet: false,
        scrollRestoreTechnique: 'cookie',
        injectChanges: false,
        minify: false,
        // logLevel: "silent",
        notify: {
            styles: {
                background: '#2d2d2d',
                padding: "10px 15px",
                top: '15px',
                right: '15px',
                color: '#ccc',
                fontSize: '12px',
                borderRadius: "5px",
                fontFamily: "monospace",
            }
        },

        // lze povolit/zakazat jednotlive synchronizace
        // ghostMode: {
        //     clicks: true,
        //     forms: true,
        //     scroll: false
        // },

    });
});

// favicons
gulp.task('favicons', function () {
    return gulp
        .src(paths.favicons.src)
        .pipe(gulp.dest(paths.favicons.dest));
});


// PHP to HTML
gulp.task('php2html', function () {
    return gulp
        .src(paths.php2html.src)
        .pipe(php2html())
        .pipe(gulp.dest(paths.php2html.dest)).on('end', function () {
            browserSync.reload();
        });
});

// Autoprefixer
gulp.task('autoprefixer', function () {
    return gulp.src(path.autoprefixer.src)
        .pipe(autoprefixer({
            browsers: ['last 2 versions', 'IE 8', 'IE 9', 'IE 10', 'IE 11', '> 1%'],
            cascade: false
        }))
        .pipe(gulp.dest('dist'));
});

// obsluha fontu - za cil se klade vykopirovat fonty a fontikony z bower_components na vystup
// clean fonts
gulp.task('clean-fonts', function () {
    return gulp.src(paths.fonts.clean, {read: false})
        .pipe(rimraf({force: true}));
});
// process fonts
gulp.task('process-fonts', function () {
    return gulp.src(paths.fonts.src)
        .pipe(gulp.dest(paths.fonts.dest));
});
// init fonts
gulp.task('fonts', gulpSequence('clean-fonts', 'process-fonts'));


// obsluha obrazku - za cil se klade obrazky vykopirovat, optimalizovat a nasypat na vystup
// nektere obrazky se musi vytahat z pluginu v bower_components (ty taky prochazi optimalizaci)
// clean images
gulp.task('clean-images', function () {
    return gulp.src(paths.images.clean, {read: false})
        .pipe(rimraf({force: true}));
});
// images fancybox process
gulp.task('fancybox-images', function () {
    return gulp.src(paths.fancybox.images.src)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(paths.fancybox.images.dest));
});
// vytvori se png sprit ze sady png ikon
// vystup jde k obrazkum na vstupu, protoze se pocita s nasledujicim taskem "process-images"
gulp.task('process-sprites', function () {
    var spriteData = gulp.src(paths.sprites.src).pipe(spritesmith({
        imgName: paths.sprites.images.name,
        cssName: paths.sprites.styles.name,
        imgPath: paths.sprites.url
    }));

    var imgStream = spriteData.img
        .pipe(gulp.dest(paths.sprites.images.dest));

    var cssStream = spriteData.css
        .pipe(gulp.dest(paths.sprites.styles.dest));

    return merge(imgStream, cssStream);
});
// process images
gulp.task('process-images', function () {
    return gulp.src(paths.images.src)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(paths.images.dest)).on('end', function () {
            browserSync.reload();
        });
});
// sprites process
gulp.task('sprites', function (cb) {
    gulpSequence('process-sprites', 'process-less', cb);
});
// task, ktery spousti kompletni sadu tasku pro cely proces zpracovani obrazku, musi mit callback (primarne kvuli browsersync)
// init & watch images
gulp.task('images', function (cb) {
    gulpSequence('clean-images', 'fancybox-images', 'sprites', 'process-images', cb);
});

// standardni watch s moznosti rozsireni browsersync notifikaci
// devel
gulp.task('watch', function () {
    gulp.watch(paths.watch.js, ['scripts']).on('change', function (file) {
        browserSync.notify("<span style='color: #99cc99;'>JS has been compiled</span>");
    });
    gulp.watch(paths.watch.less, ['process-less']).on('change', function (file) {
        browserSync.notify("<span style='color: #99cc99;'>Less has been compiled</span>");
    });
    gulp.watch(paths.watch.images, ['images']).on('change', function (file) {
        browserSync.notify("<span style='color: #99cc99;'>Images has been compressed</span>");
    });
    gulp.watch(paths.watch.favicons, ['favicons']).on('change', function (file) {
        browserSync.notify("<span style='color: #99cc99;'>Favicons has been compressed</span>");
    });
    gulp.watch(paths.watch.php2html, ['php2html']).on('change', function (file) {
        browserSync.notify("<span style='color: #99cc99;'>Html has been compressed</span>");
    });
    // gulp.watch(paths.watch.latte, ['latte']).on('change', function (file) {
        browserSync.notify("<span style='color: #99cc99;'>Latte has been edited</span>");
    // });
});


// preprocess jsou tasky zamerene na vytahnuti stylu z pluginu v bower_components a upravu cest pro vystup
// vystupy se ukladaji do slozky assets/preprocess/ - ty se pak importuji v components.less, proto se musi task "process-less" spustit jako posledni
gulp.task('sablona', function () {

    function getArg(key) {
      var index = process.argv.indexOf(key);
      var next = process.argv[index + 1];
      return (index < 0) ? null : (!next || next[0] === "-") ? true : next;
    }

    var deleteTmp = getArg("--delete");
    var addTmp = getArg("--add");
    var updateTmp = getArg("--update");

    if(addTmp) {
        if(fs.existsSync(paths.sablona.php.dest+addTmp+'.php')) {
            return gutil.log("Template "+addTmp+" is already added.");
        } else {
            if(fs.existsSync(paths.sablona.less.src+addTmp)) { 
                var merged = mergeStream(); 
                merged.add(
                    gulp.src(paths.sablona.less.src+addTmp+'/'+addTmp+'.less')
                        .pipe(gulp.dest(paths.sablona.less.dest+'tmpls/'))
                        .on('end', function() {
                            gulp.src(paths.sablona.php.src+addTmp+'/'+addTmp+'.php')
                                .pipe(gulp.dest(paths.sablona.php.dest))
                                .on('end', function() {
                                    gulp.src(paths.sablona.js.src+addTmp+'/'+addTmp+'.js')
                                        .pipe(gulp.dest(paths.sablona.js.dest))
                                        .on('end', function() {
                                            gulp.src(paths.sablona.less.dest+'tmpls/*.less')
                                                .pipe(lessImport('tmpls.less'))
                                                .pipe(gulp.dest(paths.sablona.less.dest));
                                        });
                                });
                        })
                );
                merged;
                return gutil.log("Template "+addTmp+" was added.");
            } else {
                return gutil.log("Cannot find template "+addTmp+".");
            }
        }
    }
    else if(deleteTmp) {
        if(fs.existsSync(paths.sablona.php.dest+deleteTmp+'.php')) {
            fse.remove(paths.sablona.less.dest+'tmpls/'+deleteTmp+'.less', function (err) {
                if (err) return console.error(err);
                fse.remove(paths.sablona.php.dest+deleteTmp+'.php', function (err) {
                    if (err) return console.error(err);
                    fse.remove(paths.sablona.js.dest+deleteTmp+'.js', function (err) {
                        if (err) return console.error(err);
                        var files = fs.readdirSync(paths.sablona.less.dest+'tmpls/');
                        if(typeof files[0] === "undefined") {
                            fse.remove(paths.sablona.less.dest+'tmpls/', function (err) {
                                if (err) return console.error(err)
                            });
                        }
                        gulp.src(paths.sablona.less.dest+'tmpls/**.less')
                            .pipe(lessImport('tmpls.less'))
                            .pipe(gulp.dest(paths.sablona.less.dest));
                    });
                });
            });
            return gutil.log("Template "+deleteTmp+" was removed.");
        } else {
            return gutil.log("Template "+deleteTmp+" is not added.");
        }
    }
    else if(updateTmp) {
        if(fs.existsSync(paths.sablona.php.dest+updateTmp+'.php')) {
            fse.remove(paths.sablona.less.dest+'tmpls/'+updateTmp+'.less', function (err) {
                if (err) return console.error(err);
                fse.remove(paths.sablona.php.dest+updateTmp+'.php', function (err) {
                    if (err) return console.error(err);
                    fse.remove(paths.sablona.js.dest+updateTmp+'.js', function (err) {
                        if (err) return console.error(err);
                        if(fs.existsSync(paths.sablona.less.src+updateTmp)) { 
                            var merged = mergeStream(); 
                            merged.add(
                                gulp.src(paths.sablona.less.src+updateTmp+'/'+updateTmp+'.less')
                                    .pipe(gulp.dest(paths.sablona.less.dest+'tmpls/'))
                                    .on('end', function() {
                                        gulp.src(paths.sablona.php.src+updateTmp+'/'+updateTmp+'.php')
                                            .pipe(gulp.dest(paths.sablona.php.dest))
                                            .on('end', function() {
                                                gulp.src(paths.sablona.js.src+updateTmp+'/'+updateTmp+'.js')
                                                    .pipe(gulp.dest(paths.sablona.js.dest))
                                                    .on('end', function() {
                                                        gulp.src(paths.sablona.less.dest+'tmpls/*.less')
                                                            .pipe(lessImport('tmpls.less'))
                                                            .pipe(gulp.dest(paths.sablona.less.dest));
                                                    });
                                            });
                                    })
                            );
                            merged;
                            return gutil.log("Template "+updateTmp+" was updated.");
                        } else {
                            return gutil.log("Cannot find template "+updateTmp+".");
                        }
                    });
                });
            });
        } else {
            return gutil.log("Template "+updateTmp+" is not added.");
        }
    }
});

// fancybox-preprocess
gulp.task('fancybox-preprocess', function () {
    return gulp.src(paths.fancybox.preprocess.src)
        .pipe(concat(paths.base))

        .pipe(replace("url('", "url('../images/fancybox/"))
        .pipe(gulp.dest(paths.fancybox.preprocess.dest));
});

// process less
gulp.task('process-less', function () {
    var isErr = false;

    return gulp.src(paths.less.src)
        .pipe(sourcemaps.init())
        .pipe(less().on('error', function (err) {
            isErr = true;
            gutil.log(err.message);
            browserSync.notify("Error has occurred during compiling of Less: <span style='color: #f99157;'>" + err.message + "</span>", 10000);
            this.emit('end');
        }))
        .pipe(sourcemaps.write(paths.base))
        .pipe(gulp.dest(paths.less.dest)).on('end', function () {
            if (isErr === false) {
                browserSync.reload();
            }
        });
});
// init less
// gulp.task('less', gulpSequence('fancybox-preprocess', 'slick-preprocess', 'process-less'));
gulp.task('less', gulpSequence('fancybox-preprocess', 'process-less'));


// init scripts
gulp.task('scripts', function () {
    var isErr = false;

    return gulp.src(paths.scripts.src)
        .pipe(sourcemaps.init())
        .pipe(concat(paths.scripts.name))
        .pipe(uglify({
            mangle: false,
            output: {
                beautify: true
            }
        }).on('error', function (err) {
            isErr = true;
            gutil.log(err);
            browserSync.notify("Error has occurred during compiling of JS: <span style='color: #f99157;'>" + err.message + "</span>", 10000);
            this.emit('end');
        }))
        .pipe(sourcemaps.write(paths.base))
        .pipe(gulp.dest(paths.scripts.dest)).on('end', function () {
            if (isErr === false) {
                browserSync.reload();
            }
        });
});

gulp.task('jquery', function () {
    var isErr = false;

    paths.scripts.jquery.paths.forEach(function (path) {
        gulp.src(path.src)
            .pipe(sourcemaps.init())
            .pipe(concat(path.name))
            .pipe(uglify({
                mangle: false,
                output: {
                    beautify: true
                }
            }).on('error', function (err) {
                isErr = true;
                gutil.log(err);
                browserSync.notify("Error has occurred during compiling of JS: <span style='color: #f99157;'>" + err.message + "</span>", 10000);
                this.emit('end');
            }))
            .pipe(sourcemaps.write(paths.base))
            .pipe(gulp.dest(path.dest)).on('end', function () {
            if (isErr === false) {
                browserSync.reload();
            }
        });
    });
});

gulp.task('owlcarousel', function () {
    var isErr = false;
    var path = paths.owlcarousel;

    gulp.src(path.src)
        .on('error', function (err) {
            isErr = true;
            gutil.log(err);
            browserSync.notify("Error has occurred during compiling of JS: <span style='color: #f99157;'>" + err.message + "</span>", 10000);
            this.emit('end');
        })
        .pipe(gulp.dest(path.dest)).on('end', function () {
        if (isErr === false) {
            browserSync.reload();
        }
    });
});


// "stageless" task musi byt identicky jak "less" task, akorat nastaveni kompilace je odlisne
// init stage less
gulp.task('stageless', gulpSequence('fancybox-preprocess', 'stage-less'));
// process stage less
gulp.task('stage-less', function () {
    return gulp.src(paths.less.src)
        .pipe(less().on('error', function (err) {
            gutil.log(err);
            this.emit('end');
        }))
        .pipe(rename(paths.less.stage))
        .pipe(cssshrink())
        .pipe(gulp.dest(paths.less.dest));
});


// init stage scripts
gulp.task('stagejs', function () {
    return gulp.src(paths.scripts.src)
        .pipe(concat(paths.scripts.stage))
        .pipe(uglify({
            mangle: true
        }).on('error', function (err) {
            gutil.log(err);
            this.emit('end');
        }))
        .pipe(gulp.dest(paths.scripts.dest));
});
